var OutgoingCallRequest = Parse.Object.extend('OutgoingCallRequest');
var VirtualNumber = Parse.Object.extend('VirtualNumber');

Parse.Cloud.beforeSave(VirtualNumber, function(request, response) {
  if (!request.object.get('phoneNumber')) {
    response.error('virtual numbers must have a phone number'); return;
  }

  if (!request.object.get('incomingCallUrl')) {
    response.error('virtual numbers must have an incoming call url'); return;
  }
  if (!request.object.get('incomingCallMethod')) {
    response.error('virtual numbers must have an incoming call method'); return;
  }
  
  if (!request.object.get('outgoingCallUrl')) {
    request.object.set('outgoingCallUrl', 'http://twimlets.com/forward');
    request.object.set('outgoingCallMethod', 'POST');
  }
  if (!request.object.get('outgoingCallMethod')) {
    response.error('you have set an outgoing call url but not an outgoing call method'); return;
  }

  response.success();
});

// Server for custom web hooks
(function() {
  var twilio = require('twilio');
  var url = require('url');
  var express = require('express');

  var app = express();
  app.use(express.urlencoded());

  // Look to see if there are any outgoing call requests pending from the From
  // number through the To number.
  //
  // If there is, the From number is the device phone number. Delete the
  // request and Redirect to the configured outgoing call URL for the To
  // number, appending a PhoneNumber parameter with the stored phone number to
  // call and a CallerId parameter with the To number.
  //
  // If there isn't, Redirect to the configured incoming call URL for the To
  // number.
  app.all('/alpha/twilio/voice', function(req, res) {
    res.setHeader('Content-Type', 'application/xml');

    var response = new twilio.TwimlResponse();
    var fromPhoneNumber = req.body.From || req.query.From;
    var toPhoneNumber = req.body.To || req.query.To;

    var virtualNumberQuery = new Parse.Query(VirtualNumber);
    virtualNumberQuery.equalTo('phoneNumber', toPhoneNumber);

    var outgoingCallRequestQuery = new Parse.Query(OutgoingCallRequest);
    outgoingCallRequestQuery.equalTo('devicePhoneNumber', fromPhoneNumber);
    outgoingCallRequestQuery.equalTo('virtualPhoneNumber', toPhoneNumber);

    Parse.Promise.when(virtualNumberQuery.first(), outgoingCallRequestQuery.first())
    .then(function(virtualNumber, outgoingCallRequest) {
      if (!virtualNumber) {
        response.say("This is an error message. The number you have called is not registered as a virtual number with Voksa.");
      } else {
        if (outgoingCallRequest) {
          outgoingCallRequest.destroy(); // TODO error handling on this
          var outgoingCallUrl = virtualNumber.get('outgoingCallUrl');
          var outgoingCallMethod = virtualNumber.get('outgoingCallMethod');
          var phoneNumberToCall = outgoingCallRequest.get('phoneNumberToCall');
          if (!outgoingCallUrl) {
            response.say("This is an error message. No outgoing call URL for this virtual number.");
          } else if (!outgoingCallMethod) {
            response.say("This is an error message. No outgoing call method for this virtual number.");
          } else if (!phoneNumberToCall) {
            response.say("This is an error message. No phone number to call for this outgoing call request.");
          } else {
            var parsedUrl = url.parse(outgoingCallUrl, true, true);
            delete parsedUrl.search;
            parsedUrl.query.PhoneNumber = phoneNumberToCall;
            parsedUrl.query.CallerId = toPhoneNumber;
            response.redirect({method: outgoingCallMethod}, url.format(parsedUrl));
          }
        } else {
          var incomingCallUrl = virtualNumber.get('incomingCallUrl');
          var incomingCallMethod = virtualNumber.get('incomingCallMethod');
          if (!incomingCallUrl) {
            response.say("This is an error message. No incoming call URL for this virtual number.");
          } else if (!incomingCallMethod) {
            response.say("This is an error message. No incoming call method for this virtual number.");
          } else {
            response.redirect({method: incomingCallMethod}, incomingCallUrl);
          }
        }
      }
      res.send(response.toString());
    }, function(error) {
      response.say("This is an error message. Error loading information for this phone number.");
      res.send(response.toString());
    });
  });

  app.listen();
})();